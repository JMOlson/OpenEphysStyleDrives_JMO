#OpenEphysStyleDrives_JMO
This repo contains designs for 3D printing tetrode hyperdrives based off of the shuttledrive design from Open Ephys (link:https://open-ephys.org/shuttledrive). This design was modified to fit the SpikeGadgets 256 channel EIB by John Bladon before the start of this repo - his explanation of changes can be found in readme_startingpoint.md.
