OpenEphys-SpikeGadgets Readme;
The point of this readme is to explain the OE-SG drive, but also to catalog the updates I’ve made to it


OE-SG drive Build and rationale:

OE-SG updates:
1.	First attempt: just use adapter board
a.	Print the oe drive as is, and build an adapter board using eagle
b.	Prints come from protolabs, but they’re about $60 a drive and the shuttles are about $100 per 64 (you can make a larger pallet though and that makes it easier
c.	The adapter board is difficult, the build parameters file needs to be made with very small vias
2.	Second attempt: reshape the oe drive to the sg board
a.	Built a pretty to-the-books drive, the parameters in the voigts drive were poorly defined, so I had to use a lot of guess work
b.	I used a 10 micron overlap between the channels and the shuttles, that was way too much overlap
c.	The overall diameter was a bit wide for the board, and the post processing wasn’t great
3.	Third attempt: fine tuning
a.	Increased the channel to shuttle clearance so now its beveled inwards, and its even at inner, but theres a 10 micron gap at the outer
b.	Brought in and up the top of the drive to better fit the board
c.	Rounded out some sharp corners
d.	Elongated the shuttles and built forks on the top
e.	After print, I left to dry for three hours- not a good idea.
i.	The forks had all dried shut, many of the bottom lead screw holes had dried shut
ii.	This was also worse farther down the print, probably because it was out of the solution for longer.
f.	Major design principle: keep your prints SHORT!!!!! That way there are further layer strokes.  XY dimensions build about 20 times faster than the z dimension.  So the z should be high res, but also really small
g.	This update was called v3_3prong68lane is now Drive_Body_v5
h.	This jig drive holder inner is now called jig drive holder inner v5

 
Modifications from OE Drive to SG drive:
1.	Increased overall diameter to fit SG EIB. (changed from 42 to52 mm outer lip)
2.	Modified bottom mating piece to accommodate wider spread of guide cannulae
3.	Increased diameter of through holes to house lead screws: top hole increased from 0.72 to 0.82 (screw dia is 0.8): bottom hole increased from 0.48 to 0.55 (screw dia is 0.5)
4.	Modified fit for shuttle in slot: was rectangle fit overlap (slot: 1.14mm shuttle: 1.16mm) now is drafted fit: (1o draft, slot in:1.14 slot out: 1.35 shuttle: 1.16). Hollow neck of shuttle will squeeze to 1.14, whereas the threaded base may expand to ~1.2
5.	Shortened distance from bed of neck to floor of slot so more of bare lead will be hidden (was 14.2, now is just 14mm (screw distance=14)
6.	Changed how I made the hook on the shuttles to be printed on a form printer
7.	Reshaped the shuttles:
a.	For black, overall length is ~15mm, thread hole is 0.77, slot is 0.4d, hook is .25 thick and .35 slot width.  Black probably makes hook the best, but material is rather soft
b.	For grey, overall length is 15mm, ,thread hole is 0.77, slot is 0.5d, hook is .2875 thick and .2875 slot width. Noticed more ragging with grey, so grew slot and thickened hook
c.	Shuttles will absolutely not work in clear
8.	Formlabs suggests we use the glass infused or grey pro for the shuttles, and durable for the jig.
9.	2-5-2021:
a.	lengthened the wide set for the bare lead of the screws because the tight-fitting hole was often plugged
b.	Increased to three barbs on drive and increased their length to extend beyond outer rim
c.	Updated drive body holder inner to seat drive better and to hold with three arms
d.	Increased lane count to 68 to allow for errors and to allow us to crash out a double lane for optic fibers
10.	4-14-2021
a.	Built a drive guide protector
b.	Drive guides now have a screw thread on their outside so that you can screw on the protector
c.	Added a cannula guide, this is to be inserted once the guide and drive body are fit in order to organize the cannulae.
d.	This is also to serve as a barrier so that shuttles nolonger bend the silica when driving down through a stuck cannula
e.	Added many edits to the SOP, including procedures on cutting canulae (this has been causing us problems)

Revamping the jig:
Jig 1:
1.	Reengineered the eib holder outer and inner to accommodate a wider diameter drive
2.	Rebuilt the eib holder and upper to hold sg eib with a press-tab fitting and to have a large diameter barrel for stability (we press hard on the EIB)
3.	All the dowel holes and screw holes needed to be resized
4.	Remade jig inner so that it now has a snap fit to the drive (drive now has tabs on its outer.  I think I’ll use these for the protective hat once I build it)
5.	Increased thickness of drive outer portion
6.	Remade the jig bottom for filament printer and for larger size drive
